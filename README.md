# Multipass
Multipass web application

## Public urls


dev: [https://create.multipass.org/](https://create.multipass.org/)


## Backend url: no back

## Installation

Use the package manager [yarn](https://yarnpkg.com/) to install multipass.

```bash
yarn
```

## Run app
```bash
yarn watch
```

Runs the app in the development mode.
Open http://localhost:3003 to view it in the browser.

## Build

```bash
yarn build
```

## Build by Envs

Please check package.json & scripts/server.js for using ports of dev, stage, prod env
After build you can start every env via following command
```bash
yarn start dev
yarn start stage
yarn start prod
```
## Fix code style
```bash
yarn format
```
