const express = require("express");
const path = require("path");
const compression = require("compression");
const logger = require("../config/logger/logger");
const app = express();

app.use(compression());
app.use(logger);

if (process.env.NODE_ENV === "production") {
  // Serve any static files
  app.use(express.static(path.join(__dirname, "../", "build")));
  // Handle React routing, return all requests to React app
  app.get("*", (request, response) => {
    response.sendFile(path.join(__dirname, "../", "build", "index.html"));
  });
}
const port = process.env.PORT || 3003;
app.listen(port, () => {
  console.log(`Server listening on port ${port}...`);
});
