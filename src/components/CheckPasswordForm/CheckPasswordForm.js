import React, { useEffect } from "react";
import {Form} from "react-final-form";
// eslint-disable-next-line import/no-cycle
import { DropZone, PasswordInput, FormButton } from "..";
import { notificationTypes } from "../../assets/enums";
import { useTitle } from "../../assets/helpers";
import { validate } from "../../routes/CheckPass/check-pass.config";

export const CheckPasswordForm = ({ title, store, handleGetFields }) => {
  useEffect(() => {
    useTitle(title);
  }, []);

  const setItem = (name, value) => {
    store.checkMultipass.setMultipass(name, value);
  };

  const removeMultipass = () => {
    store.checkMultipass.deleteMultipass();
  };

  const handleSubmit = () => {
    const { checkMultipass, notificationStore } = store;

    if (!checkMultipass.isMultipassUploaded()) {
      notificationStore.setNotification({
        title: "Multipass field is empty",
        message: "Please, upload your Multipass!",
        type: notificationTypes.error,
      });
    } else {
      setTimeout(() => {
        checkMultipass.unpackMultipass(showFields);
      }, 0);
    }
  };

  const showFields = (status, fields) => {
    const { notificationStore } = store;

    if (status === "success") {
      notificationStore.setNotification({
        title: "Success!",
        message: "Your Multipass file was unpacked!",
      });

      handleGetFields(fields, true);
    } else {
      notificationStore.setNotification({
        title: "Your Multipass can't be unpacked!",
        message: "Check your Multipass or password validity",
        type: notificationTypes.error,
      });
    }
  };

  return (
    <Form
      onSubmit={handleSubmit}
      validate={validate}
      render={({ handleSubmit }) => (
        <form className="check-pass" onSubmit={handleSubmit} noValidate>
          <h2 className="check-pass-heading">
            Please, upload your Multipass to check your data
          </h2>
          <DropZone
            maxFiles={1}
            name="multipassZip"
            setItem={setItem}
            accept=".zip,.rar,.7zip/*"
            removeItem={removeMultipass}
          />
          <p className="check-pass-field-heading">
            Specify password for your Multipass
          </p>
          <PasswordInput
            placeholder="Enter your password"
            setItem={setItem}
          />
          <div className="flex center">
            <FormButton text="Unpack your Multipass" />
          </div>
        </form>
      )}
    />
  );
}
