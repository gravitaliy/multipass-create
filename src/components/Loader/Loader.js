import React from "react";
import { observer } from "mobx-react";
import "./Loader.scss";

export const Loader = observer(({ store }) => {
  return (
    store.loader.isLoading && (
      <div className="loading-wrap">
        <div className="loading" />
        <div className="loading-text-wrap">
          <div className="loading-text">{store.loader.text}</div>
        </div>
      </div>
    )
  );
});
