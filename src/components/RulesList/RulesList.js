import React from "react";
import "./RulesList.scss";

export const RulesList = ({ data }) => {
  return (
    <ul className="rules-list">
      {data.map(item => (
        <li className="rules-list-item" key={item.value.toString()}>
          {item.value}
        </li>
      ))}
    </ul>
  );
}
