import React from "react";
import Dropzone from "react-dropzone-uploader";
import "./DropZone.scss";

export const DropZone = ({ setItem, name, accept, removeItem, maxFiles }) => {
  const convertToArrayBuffer = file => {
    const fr = new FileReader();
    fr.readAsArrayBuffer(file);
    fr.onloadend = () => {
      const image = fr.result;
      setItem(name, image);
    };
  };

  const handleChangeStatus = ({ file }, status) => {
    if (status === "done") {
      if (accept) {
        setItem(name, file);
      } else {
        convertToArrayBuffer(file);
      }
    }
    if (status === "removed") {
      removeItem(name);
    }
  };

  return (
    <Dropzone
      onChangeStatus={handleChangeStatus}
      accept={accept || "image/*"}
      submitButtonContent={null}
      SubmitButtonComponent={null}
      inputContent="Drag&Drop files here or click to select files"
      styles={{
        dropzone: { minHeight: 100, maxHeight: 120, overflow: "hidden" },
      }}
      maxFiles={maxFiles}
    />
  );
};
