import React from "react";
import { observer } from "mobx-react";
import {
  NotificationManager,
  NotificationContainer,
} from "react-notifications";
import "react-notifications/lib/notifications.css";

export const NotificationComponent = observer(({ store }) => {
  const { notification } = store;
  const { title, message, type, duration } = notification;

  if (type) {
    NotificationManager[type](title, message, duration, () => {});
  }

  return <NotificationContainer />;
});
