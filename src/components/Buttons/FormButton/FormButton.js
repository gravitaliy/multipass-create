import React from "react";
import "./FormButton.scss";

export const FormButton = ({ text, color }) => {
  return (
    <input
      type="submit"
      className="form-btn"
      style={{ backgroundColor: color }}
      value={text}
    />
  );
};
