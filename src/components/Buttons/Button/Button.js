import React from "react";
import clsx from "clsx";
import "./Button.scss";

export const Button = ({
  text,
  color,
  bgColor,
  disabled,
  onClick,
  styles,
  className,
}) => {
  const style = styles || {};

  return (
    <button
      type="button"
      className={clsx("btn", { disabled, [className]: !!className })}
      style={{
        backgroundColor: bgColor,
        color,
        ...style,
      }}
      onClick={onClick}
      disabled={disabled}
    >
      {text}
    </button>
  );
};
