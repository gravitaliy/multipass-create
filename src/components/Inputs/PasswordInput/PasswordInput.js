import React, { useState } from "react";
import { Field } from "react-final-form";
import clsx from "clsx";
import close from "./assets/closeHover.svg";
import open from "./assets/openHover.svg";
import "./PasswordInput.scss";

export const PasswordInput = ({ setItem, value, placeholder}) => {
  const [isActive, setIsActive] = useState(false);
  const [eye, setEye] = useState(close);
  const [type, setType] = useState("password");

  const handleSetItem = input => e => {
    input.onChange(e);
    setItem(e.target.name, e.target.value);
  };

  const setFocusInput = () => {
    setIsActive(true);
  };

  const setBlurInput = () => {
    setIsActive(false);
  };

  const hanldeClick = () => {
    const eyeState = eye === close;
    setEye(eyeState ? open : close);
    setType(eyeState ? "text" : "password");
  };

  return (
    <Field name="password">
      {({ input, meta }) => {
        const isError = meta?.error && meta?.touched;

        return (
          <>
            <div className="password-input-component flex">
              <input
                className={clsx("password-input-component-item", isError && "error")}
                type={type}
                name="password"
                onChange={handleSetItem(input)}
                onFocus={setFocusInput}
                onBlur={setBlurInput}
                value={value}
                placeholder={placeholder}
                minLength={8}
                maxLength={30}
                required
              />
              <div
                className={clsx(
                  "password-input-component-icon-wrap",
                  "flex",
                  "align-center",
                  isActive && "password-input-component-icon-wrap--active",
                  isError && "error"
                )}
              >
                <button
                  className="password-input-component-btn flex align-center "
                  type="button"
                  onClick={hanldeClick}
                >
                  <img
                    className="password-input-component-icon"
                    src={eye}
                    alt="eye"
                  />
                </button>
              </div>
            </div>
            {isError && <div className="password-input-component-error-text">{meta.error}</div>}
          </>
        );
      }}
    </Field>
  );
}
