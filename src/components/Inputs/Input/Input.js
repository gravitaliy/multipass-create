import React from "react";
import { Field } from "react-final-form";
import clsx from "clsx";
import "./Input.scss";

export const Input = props => {
  const setItem = input => e => {
    input.onChange(e);
    props.setItem(e.target.name, e.target.value);
  };

  return (
    <Field name={props.name}>
      {({ input, meta }) => {
        const isError = meta?.error && meta?.touched;

        return (
          <div className="input-component">
            <p className="input-component-heading">
              {props.heading}{props.required && "*"}
            </p>
            <div className="input-component-item-wrap flex start">
              <input
                /* eslint-disable-next-line react/jsx-props-no-spreading */
                {...input}
                className={clsx("input-component-item", isError && "error")}
                onChange={setItem(input)}
                value={props.value}
                name={props.name}
                required={!!props.required}
                type={props.type || "text"}
                minLength={props.minLength || 1}
                maxLength={200}
                pattern={props.pattern || ".*"}
                title={props.title || ""}
                placeholder={props.placeholder}
                disabled={props.disabled}
                onBlur={props.onBlur}
              />
            </div>
            {isError && <div className="input-component-error-text">{meta.error}</div>}
          </div>
        );
      }}
    </Field>
  );
};
