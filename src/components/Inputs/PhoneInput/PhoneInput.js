import React, { useEffect, useState } from "react";
import ReactPhoneInput from "react-phone-input-2";
import "./PhoneInput.scss";
import "./PhoneInputStyles.scss";

export const PhoneInput = ({ name, value, setItem, heading, required }) => {
  const [phone, setPhone] = useState(value || "");

  useEffect(() => {
    document.querySelector(".flag").setAttribute("class", "flag us");
    document
      .querySelector(".react-tel-input .form-control")
      .removeAttribute("placeholder");
  }, []);

  const handleOnChange = value => {
    setPhone(value);
    setItem(name, value);
  };

  return (
    <div className="phone-input-component">
      <p className="input-component-heading">
        {heading}
        {!!required && "*"}
      </p>
      <ReactPhoneInput
        inputProps={{
          name,
          required,
          autoFocus: false,
        }}
        defaultCountry="us"
        value={phone}
        onChange={handleOnChange}
      />
    </div>
  );
}
