import React from "react";
import Select from "react-select";
import { Field } from "react-final-form";
import "./CustomSelect.scss";

export const CustomSelect = ({ name, value, data, heading, required, setItem, isSearchable }) => {
  const customStyles = isError => {
    return {
      control: (styles, { isFocused }) => ({
        cursor: "pointer",
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-between",
        backgroundColor: "#ffffff",
        borderWidth: 1,
        borderStyle: "solid",
        borderRadius: 4,
        // eslint-disable-next-line no-nested-ternary
        borderColor: isError
          ? "#ff0000"
          : isFocused
            ? "#85b7d9"
            : "rgba(34, 36, 38, 0.15)",
        transition: "border-color 0.1s ease",
      }),
      indicatorsContainer: provided => ({
        ...provided,
        "& > *": {
          color: "#000000 !important",
        }
      }),
    };
  }


  const handleSetItem = input => (value, item) => {
    console.log(value.value);
    input.onChange(value.value);
    setItem(item.name, value.value);
  };

  return (
    <Field name={name}>
      {({ input, meta }) => {
        const isError = meta?.error && meta?.touched;

        return (
          <div className="select-component">
            <p className="select-component-heading">
              {heading}
              {!!required && "*"}
            </p>
            <div className="select-component-item-wrap">
              <Select
                name={name}
                value={data.filter(option => option.value === value)}
                onChange={handleSetItem(input)}
                options={data}
                isSearchable={isSearchable || false}
                styles={customStyles(isError)}
                placeholder={`Choose the ${heading.toLowerCase()}`}
                components={{
                  IndicatorSeparator: () => null,
                }}
              />
            </div>
            {isError && <div className="select-component-error-text">{meta.error}</div>}
          </div>
        );
      }}
    </Field>
  );
}
