import React from "react";
import DatePicker from "react-date-picker";
import { Field } from "react-final-form";
import clsx from "clsx";
import "./DayPicker.scss";

export const DayPicker = ({ name, value, setItem, heading, minDate, maxDate, required }) => {
  const onChange = input => date => {
    input.onChange(date === null ? "" : date);
    setItem(name, date);
  };

  return (
    <Field name={name}>
      {({ input, meta }) => {
        const isError = meta?.error && meta?.touched;

        return (
          <div className={clsx("day-picker-wrap", isError && "error")}>
            <p className="input-component-heading">
              {heading}
              {!!required && "*"}
            </p>
            <DatePicker
              onChange={onChange(input)}
              value={value}
              format="dd/MM/yyyy"
              locale="en"
              minDate={
                minDate || new Date("01 January 1900 00:00:00 UTC")
              }
              maxDate={maxDate || new Date("01 January 30 00:00:00 UTC")}
              required={!!required}
            />
            {isError && <div className="day-picker-error-text">{meta.error}</div>}
          </div>
        );
      }}
    </Field>
  );
}
