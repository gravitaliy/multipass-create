import React from "react";

export const Logo = () => {
  return (
    <a href="/">
      <img className="fix-img" src={require("./assets/logo.svg")} alt="logo" />
    </a>
  );
};
