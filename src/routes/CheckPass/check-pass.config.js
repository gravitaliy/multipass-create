import { onValidatePassword } from "../../assets/helpers/scripts/validator.utils";

export const validate = values => {
  const errors = {};

  onValidatePassword(errors, values);

  return errors;
};
