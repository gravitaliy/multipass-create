import React from "react";
import "./UnpackedFields.scss";

export const UnpackedFields = ({ fields }) => {
  return (
    <div className="unpacked-fields-mobile">
      <div className="unpacked-fields-wrap">
        {fields.map(field => (
          <div className="unpacked-fields" key={field.name}>
            <div className="unpacked-fields__heading">
              <div>{field.name}</div>
            </div>
            {field.type === "file" ? (
              <div className="unpacked-fields__img">
                <img
                  className="unpacked-fields__img"
                  src={field.value}
                  alt={field.name}
                />
              </div>
            ) : (
              <div className="unpacked-fields__text">{field.value}</div>
            )}
          </div>
        ))}
      </div>
    </div>
  );
};
