import React, { useState } from "react";
import { UnpackedFields } from "./components";
import { CheckPasswordForm } from "../../components";
import "react-notifications/lib/notifications.css";
import "./CheckPass.scss";

export const CheckPass = ({ store }) => {
  const [fields, setFields] = useState([]);
  const [isUnpacked, setIsUnpacked] = useState(false);

  const handleGetFields = (fields, isUnpacked) => {
    setFields(fields);
    setIsUnpacked(isUnpacked);
  };

  return (
    <div className="check-pass-wrap">
      <CheckPasswordForm
        store={store}
        handleGetFields={handleGetFields}
      />
      {isUnpacked && <UnpackedFields fields={fields} />}
    </div>
  );
}
