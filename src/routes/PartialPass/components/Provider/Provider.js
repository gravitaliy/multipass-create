import React, { useState } from "react";
import { FormButton } from "../../../../components";
import "./Provider.scss";

export const Provider = ({ logos, providerName, changeStep, nextStep }) => {
  const [value, setValue] = useState("");
  const [isError, setIsError] = useState(false);

  const handleChangeStep = event => {
    event.preventDefault();
    if (!value) {
      setIsError(true);
      return;
    }
    changeStep(nextStep);
  };

  const handleClick = event => {
    setValue(event.target.value);
    setIsError(false);
  };

  return (
    <div className="provider-wrap flex center">
      <form
        className="provider flex center column"
        onSubmit={handleChangeStep}
      >
        <h2 className="provider__heading">
          Choose your {providerName} provider
        </h2>
        <div className="provider-logos flex">
          {logos.map(item => (
            <div
              className="provider-logos-item flex column center"
              key={item.id}
            >
              <img
                className="provider-logos-item__img"
                src={item.img}
                alt={item.label}
              />
            </div>
          ))}
        </div>
        <div className="provider-logos-radiogroup-wrap">
          <div className="provider-logos-radiogroup grid">
            {logos.map(item => (
              <div className="flex center">
                <input
                  type="radio"
                  value={item.id}
                  name="radio"
                  onClick={handleClick}
                />
              </div>
            ))}
          </div>
        </div>
        <div className="provider__btn flex center column">
          <FormButton text="Next step" />
          {isError && (
            <p className="provider__error">Please, choose {providerName} </p>
          )}
        </div>
      </form>
    </div>
  );
}
