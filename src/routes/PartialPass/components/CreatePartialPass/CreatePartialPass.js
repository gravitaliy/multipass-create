import React from "react";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";
import { FirstStep, ThirdStep, FifthStep } from "../../../CreatePass/сomponents";
import { FormButton } from "../../../../components";
import "./CreatePartialPass.scss";

const steps = 3;

export const CreatePartialPass = ({ store }) => {
  const handleSubmit = e => {
    e.preventDefault();
    // loading start TODO
    setTimeout(() => {
      const createMultipassResult = store.createMultipass();
      if (createMultipassResult === true) {
        NotificationManager.success(
          "Success!",
          "Your Multipass file was created!",
        );
      } else {
        createMultipassResult.map(error =>
          NotificationManager.error(error.title, error.message, 5000, () => {}),
        );
      }
    }, 0);
  };

  const setFirstStepItem = (name, value) => {
    store.setFirstStepItem(name, value);
  };

  const removeFirstStepItem = name => {
    setFirstStepItem(name, "");
  };

  const setThirdStepItem = (name, value) => {
    store.setThirdStepItem(name, value);
  };

  const setFifthStepItem = (name, value) => {
    store.setFifthStepItem(name, value);
  };

  return (
    <div className="create-pass-wrap flex center column">
      <form className="create-pass" onSubmit={handleSubmit}>
        <FirstStep
          store={store.firstStep}
          setItem={setFirstStepItem}
          removeItem={removeFirstStepItem}
          steps={steps}
          stepNumber={1}
          isPassportFirst
        />
        <ThirdStep
          store={store.thirdStep}
          setItem={setThirdStepItem}
          steps={steps}
          stepNumber={2}
        />
        <FifthStep
          store={store.fifthStep}
          setItem={setFifthStepItem}
          steps={steps}
          stepNumber={3}
        />
        <div className="flex center">
          <FormButton text="Generate your Multipass" />
        </div>
      </form>
      <NotificationContainer />
    </div>
  );
}
