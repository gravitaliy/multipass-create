import React, { useEffect, useState } from "react";
import { useTitle } from "../../assets/helpers";
import { CreatePartialPass, Provider } from "./components";
import "./PartialPass.scss";

const serviceLogos = [
  {
    img: require("./assets/bitchange-logo.svg"),
    label: "bitchange",
    id: 1,
  },
  {
    img: require("./assets/marriott-logo.png"),
    label: "mariott",
    id: 2,
  },
];

const verifierLogos = [
  {
    img: require("./assets/shufti-logo.png"),
    label: "shufti",
    id: 1,
  },
  {
    img: require("./assets/sumsub-logo.png"),
    label: "sumsub",
    id: 2,
  },
];

export const PartialPass = ({ title, store }) => {
  const [step, setStep] = useState("1");

  useEffect(() => {
    useTitle(title);
  }, []);

  const changeStep = newStep => {
    setStep(newStep);
  };

  return (
    <div className="partial-passport-wrap flex center">
      <div className="partial-passport flex column center">
        {step === "1" && (
          <Provider
            changeStep={changeStep}
            logos={serviceLogos}
            providerName="service"
            nextStep="2"
          />
        )}
        {step === "2" && (
          <Provider
            changeStep={changeStep}
            logos={verifierLogos}
            providerName="service"
            nextStep="3"
          />
        )}
        {step === "3" && (
          <CreatePartialPass store={store} />
        )}
      </div>
    </div>
  );
}
