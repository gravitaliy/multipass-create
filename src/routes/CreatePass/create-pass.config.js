import {
  onValidateLatin,
  onValidateLatinNumeric,
  onValidateLatinNumericSpaces,
  onValidateEmail,
  onValidateCronAddress,
  onValidatePassword,
  onValidateRequired,
} from "../../assets/helpers/scripts/validator.utils";

export const validate = values => {
  const errors = {};

  /* step 1 */
  onValidateLatin(errors, values, "IntName", true);
  onValidateLatin(errors, values, "IntSurname", true);
  onValidateLatin(errors, values, "MiddleName");
  onValidateRequired(errors, values, "Gender");
  onValidateLatin(errors, values, "Citizenship");
  onValidateRequired(errors, values, "DateOfBirth");
  onValidateLatinNumeric(errors, values, "IdNumber", true);
  onValidateLatinNumericSpaces(errors, values, "IssuedBy");

  /* step 2 */
  onValidateLatinNumericSpaces(errors, values, "ResRegion");
  onValidateLatinNumericSpaces(errors, values, "ResCity");
  onValidateLatinNumeric(errors, values, "ResPostalCode");
  onValidateLatinNumericSpaces(errors, values, "ResAddress");

  /* step 3 */
  onValidateEmail(errors, values, "EMail", true);

  /* step 4 */
  onValidateLatinNumeric(errors, values, "AddIdNumber1");

  /* step 5 */
  onValidateCronAddress(errors, values, "CronAddress");
  onValidatePassword(errors, values);

  return errors;
};
