import React from "react";
import { observer } from "mobx-react";
import { Input, PhoneInput } from "../../../../components";
import { emailRegex, emailError } from "../../../../assets/helpers";
import "./ThirdStep.scss";

export const ThirdStep = observer(({
  stepNumber,
  steps,
  setItem,
  store
}) => {
  return (
    <div className="third-step">
      <h2 className="create-pass-heading">
        Step {stepNumber} of {steps}: Verify your phone
        number & email
      </h2>
      <div className="third-step-fields grid">
        <PhoneInput
          heading="Phone number"
          name="Phone1"
          setItem={setItem}
          value={store.Phone1}
        />
        <Input
          heading="Email"
          name="EMail"
          setItem={setItem}
          value={store.EMail}
          type="email"
          minLength={5}
          pattern={emailRegex}
          title={emailError}
          required
        />
      </div>
    </div>
  );
});
