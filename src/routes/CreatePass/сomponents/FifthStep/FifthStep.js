import React from "react";
import { observer } from "mobx-react";
import { PasswordInput, Input } from "../../../../components";
import "./FifthStep.scss";
import { cronRegex } from "../../../../assets/helpers";

export const FifthStep = observer(({ stepNumber, steps, store, setItem }) => {
  return (
    <div className="fifth-step">
      <h2 className="create-pass-heading">
        Step {stepNumber} of {steps}: Generate your
        Multipass
      </h2>
      <div className="fifth-step-fields">
        <Input
          heading={(
            <>
              Address in{" "}
              <a
                className="fifth-step-link"
                href="https://cron.global/"
                target="_blank"
                rel="noopener noreferrer"
              >
                CRON blockchain
              </a>
            </>
          )}
          name="CronAddress"
          setItem={setItem}
          value={store.CronAddress}
          pattern={cronRegex}
          title="Enter valid Cron address"
        />
        <p className="fifth-step-heading">
          Specify password for your Multipass*
        </p>
        <div className="fifth-step-input-wrap">
          <PasswordInput
            value={store.password}
            setItem={setItem}
            placeholder="Password (Min. 8 characters)"
          />
        </div>
      </div>
    </div>
  );
});
