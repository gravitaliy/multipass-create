import React from "react";
import { observer } from "mobx-react";
import { Input, CustomSelect } from "../../../../components";
import { DocumentsArea } from "./components";
import { countryList } from "../../../../assets/enums";
import {
  // latinRegex,
  latinAndNumbersRegex,
  latinAndNumbersWithSpaceRegex,
  onlyLatinAndNumbersError,
} from "../../../../assets/helpers";
import "./SecondStep.scss";

export const SecondStep = observer(({
  stepNumber,
  steps,
  setItem,
  store,
  removeItem
}) => {
  return (
    <div className="second-step">
      <h2 className="create-pass-heading">
        Step {stepNumber} of {steps}: Verify your
        address
      </h2>
      <div className="second-step-fields grid">
        <CustomSelect
          heading="Country of residence"
          name="ResCountry"
          data={countryList}
          setItem={setItem}
          value={store.ResCountry}
          isSearchable
        />
        <Input
          heading="State / Region"
          name="ResRegion"
          setItem={setItem}
          value={store.ResRegion}
          pattern={latinAndNumbersWithSpaceRegex}
          title={onlyLatinAndNumbersError}
        />
        <Input
          heading="City / Town"
          name="ResCity"
          setItem={setItem}
          value={store.ResCity}
          pattern={latinAndNumbersWithSpaceRegex}
          title={onlyLatinAndNumbersError}
        />
        <Input
          heading="Post Code"
          name="ResPostalCode"
          setItem={setItem}
          value={store.ResPostalCode}
          pattern={latinAndNumbersRegex}
          title={onlyLatinAndNumbersError}
        />
      </div>
      <div className="second-step-one-field">
        <Input
          heading="Address"
          name="ResAddress"
          setItem={setItem}
          value={store.ResAddress}
          // pattern={latinAndNumbersRegex}
          title={onlyLatinAndNumbersError}
        />
      </div>
      <p className="bold">
        Please attach legal documents confirming your identity:
      </p>
      <DocumentsArea
        store={store}
        setItem={setItem}
        removeItem={removeItem}
      />
    </div>
  );
});
