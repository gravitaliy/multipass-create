import React from "react";
import { observer } from "mobx-react";
import { DropZone, RulesList, Input, CustomSelect } from "../../../../../../components";
import {
  additionalRequirements,
  photosRequirements,
  addressDocumentTypes,
} from "../../assets/ruleslist";
import "./DocumentsArea.scss";
import { latinRegex, onlyLatinError } from "../../../../../../assets/helpers";

export const DocumentsArea = observer(({ setItem, removeItem, store }) => {
  return (
    <div className="documents-area-wrap">
      <div className="documents-area">
        <p className="documents-area__description bold">Required photos:</p>
        <ul className="documents-area-list">
          <li className="documents-area-list__item">
            1. Passport page / ID card side showing address of registration
          </li>
          <DropZone
            name="ResAddressProofId"
            maxFiles={1}
            setItem={setItem}
            removeItem={removeItem}
          />
          <div className="documents-area-list__item">
            <RulesList data={additionalRequirements} />
          </div>
          <li className="documents-area-list__item">
            2. Additional document from the list as address confirmation:
          </li>
          <div className="documents-area-types">
            <CustomSelect
              heading="Additional document type"
              name="ResDocType"
              data={addressDocumentTypes}
              setItem={setItem}
              value={store.ResDocType}
            />
          </div>
          <div className="documents-area-types">
            {store.ResDocType === "Other" && (
              <Input
                heading="Other"
                name="ResOtherDoc"
                setItem={setItem}
                value={store.ResOtherDoc}
                pattern={latinRegex}
                title={onlyLatinError}
              />
            )}
          </div>
          <DropZone
            name="ResAddressProofAdd"
            maxFiles={1}
            setItem={setItem}
            removeItem={removeItem}
          />
        </ul>
        <p>Submitted photos must adhere to the following requirements:</p>
        <RulesList data={photosRequirements} />
      </div>
    </div>
  );
});
