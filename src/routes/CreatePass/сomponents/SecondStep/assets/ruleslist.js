export const photosRequirements = [
  { value: "Additional document must be not more than 6 months old;" },
  { value: "Photos must depict account owner;" },
  { value: "Resolution must be no less than 600x600 pixels;" },
  { value: "Photos must be crisp, not blurred;" },
  {
    value: "No part of your photo can be covered or obscured by fingers;",
  },
  { value: "All the data must be clearly visible;" },
  { value: "File size must not exceed 2 MB;" },
  { value: "Accepted formats are JPEG, JPG, PNG" },
];

export const additionalRequirements = [
  { value: "Utility bills" },
  { value: "Bank statement" },
  { value: "Tenancy agreement" },
  { value: "Tax authority letters" },
];

export const addressDocumentTypes = [
  { value: "utility_bill", label: "Utility bills", id: 1 },
  { value: "bank_statement", label: "Bank statement", id: 2 },
  { value: "bank_reference_letter", label: "Bank reference letter", id: 3 },
  { value: "rent_agreement", label: "Rent agreement", id: 4 },
  { value: "tax_bill", label: "Tax bill", id: 5 },
  { value: "other", label: "Other", id: 6 },
];
