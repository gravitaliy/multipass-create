import React from "react";
import "./AttentionMessage.scss";

export const AttentionMessage = () => {
  return (
    <div className="attention-message flex center">
      <img className="attention-message__icon" src={require("./assets/mark.png")} alt="mark" />
      <p className="attention-message__text">
        After generating you will get encrypted by password above your
        multipass.zip. For opening inside files you should use 7Zip Open Source
        software or any other payed archive software. You can download it from{" "}
        <a
          className="attention-message-link"
          href="https://www.7-zip.org/download.html"
        >
          {" "}
          Official 7Zip website
        </a>
      </p>
    </div>
  );
};
