import React, { memo } from "react";
import ReactModal from "react-modal";
import { CheckPasswordForm } from "../../../../components";

const styles = {
  modal: {
    overlay: {
      backgroundColor: "rgba(0,0,0, 0.5)",
      zIndex: 2,
    },
    content: {
      bottom: "unset",
      maxWidth: 740,
      margin: "0 auto",
      maxHeight: "65vh",
    },
  },
  closeBtn: {
    display: "block",
    width: 20,
    padding: 0,
    margin: "0 0 0 auto",
    border: "none",
    background: "transparent",
    cursor: "pointer",
  },
};

export const ModalCustom = memo(
  ({ store, showModal, handleCloseModal, handleGetFields }) => {
    return (
      <ReactModal
        isOpen={showModal}
        onRequestClose={handleCloseModal}
        ariaHideApp={false}
        contentLabel="Modal Fill with my Multipass"
        style={styles.modal}
      >
        <button
          onClick={handleCloseModal}
          type="button"
          className="modal-close-btn"
          style={styles.closeBtn}
        >
          <img src={require("./assets/cancel.svg")} alt="" />
        </button>
        <CheckPasswordForm store={store} handleGetFields={handleGetFields} />
      </ReactModal>
    );
  },
);
