import React from "react";
import { observer } from "mobx-react";
import { DayPicker, DropZone, Input, CustomSelect } from "../../../../components";
import { documentTypesList, countryList } from "../../../../assets/enums";
import {
  latinAndNumbersRegex,
  onlyLatinAndNumbersError,
  getDateNearThis,
} from "../../../../assets/helpers";
import "./FourthStep.scss";

export const FourthStep = observer(({
  stepNumber,
  steps,
  setItem,
  store,
  removeItem
}) => {
  return (
    <div className="fourth-step">
      <h2 className="create-pass-heading">
        Step {stepNumber} of {steps}: Verify your
        additional docs
      </h2>
      <div className="fourth-step-fields">
        <CustomSelect
          heading="Type of document"
          name="AddDocType1"
          data={documentTypesList}
          setItem={setItem}
          value={store.AddDocType1}
        />
        <div className="fourth-step-fields grid">
          <CustomSelect
            heading="Issuing country"
            name="AddIssuingCountry1"
            data={countryList}
            setItem={setItem}
            value={store.AddIssuingCountry1}
            isSearchable
          />
          <Input
            heading="ID number"
            name="AddIdNumber1"
            setItem={setItem}
            value={store.AddIdNumber1}
            pattern={latinAndNumbersRegex}
            title={onlyLatinAndNumbersError}
          />
          <DayPicker
            heading="Date of id issuing"
            name="AddDateIdIssuing1"
            setItem={setItem}
            value={store.AddDateIdIssuing1}
            maxDate={getDateNearThis(-1)}
          />
          <DayPicker
            heading="Date of id expiry"
            name="AddDateIdExpiry1"
            setItem={setItem}
            value={store.AddDateIdExpiry1}
            minDate={new Date()}
          />
        </div>
        <div className="additional-document-wrap">
          <div className="additional-document">
            <div className="additional-document-description">
              <p className="additional-document-description__text bold">
                Photos required
              </p>
              <p className="additional-document-description__text">
                Your Additional Document
              </p>
            </div>
            <DropZone
              name="AddFile1"
              maxFiles={1}
              setItem={setItem}
              removeItem={removeItem}
            />
          </div>
        </div>
      </div>
    </div>
  );
});
