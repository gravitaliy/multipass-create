import React from "react";
import { observer } from "mobx-react";
import { DayPicker, Input } from "../../../../components";
import { PassportArea, SelfieArea, PersonalArea } from "./components";
import {
  latinRegex,
  latinAndNumbersRegex,
  latinAndNumbersWithSpaceRegex,
  onlyLatinError,
  onlyLatinAndNumbersError,
  getDateNearThis,
} from "../../../../assets/helpers";
import "./FirstStep.scss";

export const FirstStep = observer(({
  stepNumber,
  steps,
  setItem,
  store,
  isPassportFirst,
  removeItem
}) => {
  return (
    <div className="first-step">
      <h2 className="create-pass-heading">
        Step {stepNumber} of {steps}: Verify your
        identity
      </h2>
      {isPassportFirst && (
        <>
          <PassportArea
            setItem={setItem}
            store={store}
            removeItem={removeItem}
          />
          <SelfieArea
            setItem={setItem}
            removeItem={removeItem}
          />
        </>
      )}
      <div className="first-step-fields grid">
        <PersonalArea setItem={setItem} store={store} />
        <Input
          heading="Passport / National ID number"
          name="IdNumber"
          setItem={setItem}
          value={store.IdNumber}
          pattern={latinAndNumbersRegex}
          title={onlyLatinAndNumbersError}
          required
        />
        {store.Occupation === "Other" && (
          <Input
            heading="Occupation Other"
            name="OccupationOther"
            setItem={setItem}
            value={store.OccupationOther}
            pattern={latinRegex}
            title={onlyLatinError}
          />
        )}
      </div>
      <div className="first-step-one-field">
        <Input
          heading="Issued by"
          name="IssuedBy"
          setItem={setItem}
          value={store.IssuedBy}
          pattern={latinAndNumbersWithSpaceRegex}
          title={onlyLatinAndNumbersError}
        />
      </div>
      <div className="first-step-fields grid">
        <DayPicker
          heading="Issue date"
          name="DateOfIdIssuing"
          setItem={setItem}
          value={store.DateOfIdIssuing}
          maxDate={getDateNearThis(-1)}
        />
        <DayPicker
          heading="Expire date"
          name="DateOfIdExpiry"
          setItem={setItem}
          value={store.DateOfIdExpiry}
          minDate={new Date()}
        />
      </div>
      <p className="bold">
        Please attach legal documents confirming your identity:
      </p>
      {!isPassportFirst && (
        <>
          <PassportArea
            setItem={setItem}
            store={store}
            removeItem={removeItem}
          />
          <SelfieArea
            setItem={setItem}
            removeItem={removeItem}
          />
        </>
      )}
    </div>
  );
});
