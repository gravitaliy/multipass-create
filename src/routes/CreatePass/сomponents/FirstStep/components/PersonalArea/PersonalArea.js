import React from "react";
import { observer } from "mobx-react";
import { Input, DayPicker, CustomSelect } from "../../../../../../components";
import { countryList, occupationList, genderList } from "../../../../../../assets/enums";
import { latinRegex, onlyLatinError } from "../../../../../../assets/helpers";

export const PersonalArea = observer(({ setItem, store } )=> {
  return (
    <>
      <Input
        heading="First Name"
        name="IntName"
        setItem={setItem}
        value={store.IntName}
        pattern={latinRegex}
        title={onlyLatinError}
        required
      />
      <Input
        heading="Last Name"
        name="IntSurname"
        setItem={setItem}
        value={store.IntSurname}
        pattern={latinRegex}
        title={onlyLatinError}
        required
      />
      <Input
        heading="First Name (Local characters)"
        name="Name"
        setItem={setItem}
        value={store.Name}
        required={false}
      />
      <Input
        heading="Last Name (Local characters)"
        name="Surname"
        setItem={setItem}
        value={store.Surname}
        required={false}
      />
      <Input
        heading="Middle Name"
        name="MiddleName"
        setItem={setItem}
        value={store.MiddleName}
        pattern={latinRegex}
        title={onlyLatinError}
        required={false}
      />
      <CustomSelect
        heading="Gender"
        name="Gender"
        data={genderList}
        setItem={setItem}
        value={store.Gender}
      />
      <CustomSelect
        heading="Place of birth"
        name="PlaceOfBirth"
        data={countryList}
        setItem={setItem}
        value={store.PlaceOfBirth}
        isSearchable
      />
      <Input
        heading="Citizenship"
        name="Citizenship"
        setItem={setItem}
        value={store.Citizenship}
        pattern={latinRegex}
        title={onlyLatinError}
      />
      <DayPicker
        heading="Date of Birth"
        name="DateOfBirth"
        setItem={setItem}
        value={store.DateOfBirth}
        maxDate={new Date()}
        required
      />
      <CustomSelect
        heading="Occupation"
        name="Occupation"
        data={occupationList}
        setItem={setItem}
        value={store.Occupation}
      />
    </>
  );
});
