import React, { useState } from "react";
import { observer } from "mobx-react";
import { CustomSelect, DropZone } from "../../../../../../components";
import { idTypeList } from "../../../../../../assets/enums";
import "./PassportArea.scss";

export const PassportArea = observer(({ setItem, store, removeItem }) => {
  const [isToggle, setIsToggle] = useState(false);

  const handleSetItem = (name, value) => {
    if (name === "IdType") {
      setIsToggle(value === "id_card");
    }
    setItem(name, value);
  };

  return (
    <div className="passport-area-wrap">
      <div className="passport-area">
        <div className="passport-area-description">
          <p className="passport-area-description__text bold">
            Photos required
          </p>
          <p className="passport-area-description__text">
            Your passport (International passport with or without MRZ) / ID
            card
          </p>
        </div>
        <CustomSelect
          heading="Id type"
          data={idTypeList}
          name="IdType"
          setItem={handleSetItem}
          value={store.IdType}
          required
        />
        <div className="passport-area-img-wrap">
          <img
            className="passport-area-img"
            src={require("../../assets/passport-preview.png")}
            alt="passport / ID"
          />
        </div>
        <p>
          If you dont have a passport / ID card – please upload a photo of
          another official document to verify your identity
        </p>
        <DropZone
          maxFiles={1}
          name="IdFile"
          setItem={handleSetItem}
          removeItem={removeItem}
        />
        {isToggle && (
          <>
            <p className="bold">
              Please, upload backside of your identity card
            </p>
            <div className="passport-area-img-wrap">
              <img
                className="passport-area-img"
                src={require("../../assets/backside-preview.png")}
                alt="ID backside"
              />
            </div>
            <DropZone
              maxFiles={1}
              name="BackOfIdCard"
              setItem={setItem}
              removeItem={removeItem}
            />
          </>
        )}
      </div>
    </div>
  );
});
