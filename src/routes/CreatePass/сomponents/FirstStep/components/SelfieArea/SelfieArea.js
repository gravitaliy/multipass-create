import React from "react";
import { DropZone, RulesList } from "../../../../../../components";
import { selfieRequiments } from "../../assets/ruleslists";
import "./SelfieArea.scss";

export const SelfieArea = ({ setItem, removeItem }) => {
  return (
    <div className="selfie-area-wrap">
      <div className="selfie-area">
        <div className="selfie-area-description">
          <p className="selfie-area-description__text bold">Photos required</p>
          <p className="selfie-area-description__text">
            Selfie with passport / ID card
          </p>
        </div>
        <div className="selfie-area-img-wrap">
          <img
            className="selfie-area-img"
            src={require("../../assets/selfie-preview.png")}
            alt="selfie / ID"
          />
        </div>
        <p>
          You should hold it by the edge and close to your face so that we can
          verify that this document really belongs to you. Both your face and
          the picture on the document should be clearly visible and any text on
          the document must be readable and not be covered by your fingers.
        </p>
        <DropZone
          name="Selfie"
          maxFiles={1}
          setItem={setItem}
          removeItem={removeItem}
        />
        <p className="selfie-area-description__text selfie-area-rules">
          Requirements:
        </p>
        <RulesList data={selfieRequiments} />
      </div>
    </div>
  );
};
