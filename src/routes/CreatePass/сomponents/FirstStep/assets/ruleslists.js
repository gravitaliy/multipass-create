export const selfieRequiments = [
  { value: "Photos must depict account owner;" },
  { value: "Resolution must be no less than 600x600 pixels;" },
  { value: "Photos must be crisp, not blurred;" },
  { value: "File size must not exceed 2 MB;  " },
  { value: "Accepted formats are JPEG, JPG, PNG" },
];
