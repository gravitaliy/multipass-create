import React, { useEffect, useState } from "react";
import { Form } from "react-final-form";
import createDecorator from "final-form-focus";
import { FormButton, Button } from "../../components";
import { useTitle } from "../../assets/helpers";
import { notificationTypes } from "../../assets/enums";
import {
  FirstStep,
  SecondStep,
  ThirdStep,
  ModalCustom,
  AttentionMessage,
  FourthStep,
  FifthStep,
} from "./сomponents";
import "./CreatePass.scss";
import { validate } from "./create-pass.config";

const steps = 5;

const focusOnErrors = createDecorator();

export const CreatePass = ({ title, store }) => {
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    useTitle(title);
  }, []);

  const handleSubmit = () => {
    const { notificationStore, loaderStore } = store;

    loaderStore.startLoading();
    store.multipassForm.createMultipass()
      .then(() => {
        loaderStore.stopLoading();
        notificationStore.setNotification({
          title: "Success!",
          message: "Your Multipass file was created!",
        });
      })
      .catch((error) => {
        loaderStore.stopLoading();
        notificationStore.setNotification({
          title: error.title,
          message: error.message,
          type: notificationTypes.error,
          duration: 5000,
        });
      });

/*
    setTimeout(() => {
      const createMultipassResult = store.multipassForm.createMultipass();

      console.log(createMultipassResult);

      if (createMultipassResult === true) {
        notificationStore.setNotification({
          title: "Success!",
          message: "Your Multipass file was created!",
        });
      } else {
        createMultipassResult.map(error =>
          notificationStore.setNotification({
            title: error.title,
            message: error.message,
            type: notificationTypes.error,
            duration: 5000,
          }),
        );
      }
    }, 0);
*/
  };

  const setFirstStepItem = (name, value) => {
    store.multipassForm.setFirstStepItem(name, value);
  };

  const removeFirstStepItem = name => {
    setFirstStepItem(name, "");
  };

  const setSecondStepItem = (name, value) => {
    store.multipassForm.setSecondStepItem(name, value);
  };

  const removeSecondStepItem = name => {
    setSecondStepItem(name, "");
  };

  const setThirdStepItem = (name, value) => {
    store.multipassForm.setThirdStepItem(name, value);
  };

  const setFourthStepItem = (name, value) => {
    store.multipassForm.setFourthStepItem(name, value);
  };

  const removeFourthStepItem = name => {
    setFourthStepItem(name, "");
  };

  const setFifthStepItem = (name, value) => {
    store.multipassForm.setFifthStepItem(name, value);
  };

  const handleOpenModal = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleGetFields = fields => {
    handleCloseModal();

    const steps = [
      {
        name: "firstStep",
        setItem: (name, value) => setFirstStepItem(name, value),
      },
      {
        name: "secondStep",
        setItem: (name, value) => setSecondStepItem(name, value),
      },
      {
        name: "thirdStep",
        setItem: (name, value) => setThirdStepItem(name, value),
      },
      {
        name: "fourthStep",
        setItem: (name, value) => setFourthStepItem(name, value),
      },
      {
        name: "fifthStep",
        setItem: (name, value) => setFifthStepItem(name, value),
      },
    ];

    const getStepValues = step => {
      return Object.keys(store.multipassForm[step]);
    };

    fields.forEach(field => {
      steps.forEach(step => {
        if (getStepValues(step.name).includes(field.name)) {
          step.setItem(field.name, field.value);
        }
      });
    });
  };

  const initialValues = () => {
    const { firstStep, secondStep, thirdStep, fourthStep, fifthStep } = store.multipassForm;
    const allStepsValues = {
      ...firstStep,
      ...secondStep,
      ...thirdStep,
      ...fourthStep,
      ...fifthStep
    };

    return Object.assign(
      ...Object.entries(allStepsValues).map(([key, value]) => {
        if (value !== "") return { [key]: value };
        return false;
      }),
    );
  };

  return (
    <div className="create-pass-wrap flex center column">
      <div className="create-pass-top-btn-wrap">
        <Button
          onClick={handleOpenModal}
          bgColor="#3463F8"
          text="Fill with my Multipass"
          className="block-button"
        />
      </div>
      <ModalCustom
        store={store}
        showModal={showModal}
        handleCloseModal={handleCloseModal}
        handleGetFields={handleGetFields}
      />
      <Form
        onSubmit={handleSubmit}
        decorators={[ focusOnErrors ]}
        validate={validate}
        initialValues={initialValues()}
        render={({ handleSubmit }) => (
          <form className="create-pass" onSubmit={handleSubmit} noValidate>
            <FirstStep
              store={store.multipassForm.firstStep}
              setItem={setFirstStepItem}
              removeItem={removeFirstStepItem}
              steps={steps}
              stepNumber={1}
            />
            <SecondStep
              store={store.multipassForm.secondStep}
              setItem={setSecondStepItem}
              removeItem={removeSecondStepItem}
              steps={steps}
              stepNumber={2}
            />
            <ThirdStep
              store={store.multipassForm.thirdStep}
              setItem={setThirdStepItem}
              steps={steps}
              stepNumber={3}
            />
            <FourthStep
              store={store.multipassForm.fourthStep}
              setItem={setFourthStepItem}
              removeItem={removeFourthStepItem}
              steps={steps}
              stepNumber={4}
            />
            <FifthStep
              store={store.multipassForm.fifthStep}
              setItem={setFifthStepItem}
              steps={steps}
              stepNumber={5}
            />
            <AttentionMessage />
            <div className="flex center">
              <FormButton text="Generate your Multipass" />
            </div>
          </form>
        )}
      />
    </div>
  );
};
