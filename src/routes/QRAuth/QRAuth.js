import React, { useEffect, useState } from "react";
import { MultipassLib } from "multipass-qauth-frontend-lib";
import { useTitle } from "../../assets/helpers";
import { QRContainer, ErrorsLog, FormQR } from "./components";
import { notificationTypes } from "../../assets/enums";
import "./QRAuth.scss";

export const QRAuth = ({ store, title }) => {
  const [fields, setFields] = useState({
    file: null,
    password: "",
    email: "",
    pin: "",
  });
  const [link, setLink] = useState("");
  const [errors, setErrors] = useState([]);
  const [isAuth, setIsAuth] = useState(false);
  const [pinDisabled, setPinDisabled] = useState(true);
  const [hasEmail, setHasEmail] = useState(false);

  useEffect(() => {
    useTitle(title);
  }, []);

  const setError = error => {
    setErrors([
      {
        date: new Date(),
        message: String(error.message),
      },
    ]);
  };

  const deleteFile = () => {
    store.checkMultipass.deleteMultipass();

    setFields({
      ...fields,
      file: null,
    })
  };

  const readFile = (file, getResult) => {
    const fr = new FileReader();
    fr.readAsArrayBuffer(file);
    fr.onloadend = () => {
      const image = fr.result;
      getResult(image);
    };
  };

  const uploadFile = async image => {
    const { file, password } = fields;
    return MultipassLib.uploadFile(image || file, password);
  };

  const handleChange = (name, value) => {
    store.checkMultipass.setMultipass(name, value);

    if (name === "multipassZip") {
      readFile(value, image => {
        setFields({
          ...fields,
          file: image,
        });
      });

      return;
    }

    setFields({
      ...fields,
      [name]: value,
    })
  };

  const authEmail = email => {
    const QRContainer = document.getElementById("qr-code-container");

    QRContainer.innerHTML = "";

    MultipassLib.authEmail(email)
      .then(() => {
        createSuccessNotification(
          "Successfully, click on the link or QR code to get the PIN",
        );

        const result = MultipassLib.getQRCode(
          null,
          QRContainer,
          {width: 200, height: 200},
          "https://qr-auth.wdia.org/app",
        );

        setPinDisabled(false);
        setErrors([]);
        setLink(result.link);
        setHasEmail(true);
      })
      .catch(err => {
        setError(err);
        setHasEmail(false);
        setPinDisabled(true);
      });
  };

  const handleSubmitSignUp = () => {
    store.loaderStore.startLoading();

    uploadFile()
      .then(() => {
        MultipassLib.signUp()
          .then(() => {
            setErrors([]);
            setPinDisabled(false);
            createSuccessNotification("Success sign up");
            store.unpackMultipass(showFields);
          })
          .catch(err => {
            store.loaderStore.stopLoading();
            setError(err);
          });
      })
      .catch(err => {
        store.loaderStore.stopLoading();
        setError({message: err});
      });
  };

  const handleSubmitSignIn = () => {
    if (fields.pin) {
      store.loaderStore.startLoading();

      MultipassLib.signIn(null, fields.pin)
        .then(({ data }) => {
          if (data.result[0] === 1) {
            setIsAuth(true);
            createSuccessNotification("Success sign in!");
          } else {
            createErrorNotification({
              title: "Not valid pin!",
            });
          }
        })
        .catch(err => {
          setError(err);
        })
        .finally(() => {
          store.loaderStore.stopLoading();
        });
    } else {
      createErrorNotification({
        title: "Required field",
        message: "Pin is required field, please enter!",
      });
    }
  };

  const checkEmail = () => {
    authEmail(fields.email);
  };

  const handleSubmitSignInToMultipass = () => {
    if (!store.checkMultipass.isMultipassUploaded()) {
      createErrorNotification({
        title: "Multipass field is empty",
        message: "Please, upload your Multipass!",
      });
    } else {
      setTimeout(() => {
        store.checkMultipass.unpackMultipass(showFields);
      }, 0);
    }
  };

  const showFields = (status, fields) => {
    getEmail(status, fields);

    if (status === "success") {
      const user = fields.find(item => item.name.toLowerCase() === "email");
      if (user) authEmail(user.value);
    } else {
      createErrorNotification({
        title: "Your Multipass can't be unpacked",
        message: "Check your Multipass or password validity",
      });
    }
  };

  const getEmail = (status, values) => {
    if (status === "success") {
      const user = values.find(item => item.name.toLowerCase() === "email");

      setFields({
        ...fields,
        email: user.value,
      });
    }
  };

  const createSuccessNotification = message => {
    store.notificationStore.setNotification({
      message: message || "",
    });

    setErrors([]);
  };

  const createErrorNotification = error => {
    store.notificationStore.setNotification({
      title: error.title,
      message: error.message,
      type: notificationTypes.error,
    });
  };

  if (isAuth) {
    return (
      <div className="flex center">
        <div>
          <h3 style={{marginBottom: 20, textAlign: "center"}}>
            You are logged in! Hurray!
          </h3>
          <img
            style={{width: 320}}
            src="https://avatars.mds.yandex.net/get-zen_doc/1780598/pub_5d98d2c195aa9f00aef9f996_5d98d60ee882c300b2f7a03e/scale_1200"
            alt="success"
          />
        </div>
      </div>
    );
  }

  return (
    <div className="qr-auth-wrap flex center">
      <div className="form-container">
        <h2>QR multipass</h2>
        <div className="flex justify-between">
          <div className="block-form">
            <FormQR
              onSubmitSignUp={handleSubmitSignUp}
              onSubmitSignIn={handleSubmitSignInToMultipass}
              onSubmitSignInPin={handleSubmitSignIn}
              defaultValues={fields}
              handleChange={handleChange}
              checkEmail={checkEmail}
              deleteFile={deleteFile}
              hasEmail={hasEmail}
              pinDisabled={pinDisabled}
            />
          </div>
          <div className="block-form">
            <QRContainer link={link} />
          </div>
        </div>
        <ErrorsLog errors={errors} />
      </div>
    </div>
  );
}
