import React from "react";
import "./QRContainer.scss";

export const QRContainer = ({ link }) => (
  <div className="flex justify-center h-100">
    <div>
      <div id="qr-code-container" className="qr-code-container" />
      {link && (
        <a className="link-qr" href={link} target="_blank" rel="noreferrer noopener">
          Open link
        </a>
      )}
    </div>
  </div>
);
