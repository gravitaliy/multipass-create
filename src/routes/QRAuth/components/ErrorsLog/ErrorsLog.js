import React from "react";
import { ErrorLog } from "./Error";
import "./ErrorsLog.scss";

export const ErrorsLog = ({ errors }) => (
  <div className={errors.length && "errors-log"}>
    {errors.map(error => (
      <ErrorLog
        date={error.date}
        message={error.message}
        key={error.date.toISOString()}
      />
    ))}
  </div>
);
