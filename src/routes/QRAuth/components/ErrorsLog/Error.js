import React from "react";

export const ErrorLog = ({ date, message }) => (
  <div>
    <pre>{date.toISOString()}</pre>
    <div className="error-text">{message}</div>
  </div>
);
