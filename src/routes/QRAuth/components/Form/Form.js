import React, { useState } from "react";
import { Form } from "react-final-form";
import { DropZone, Button, Input, PasswordInput } from "../../../../components";
import { validate } from "./form.config";

export const FormQR = ({
  defaultValues,
  handleChange,
  onSubmitSignUp,
  onSubmitSignIn,
  onSubmitSignInPin,
  checkEmail,
  deleteFile,
  hasEmail,
  pinDisabled,
}) => {
  const [isLogin, toggleLogin] = useState(false);

  const handleClickSignIn = () => {
    if (isLogin && !hasEmail) {
      checkEmail();
      return;
    }
    if (isLogin || !pinDisabled) onSubmitSignInPin();
    else onSubmitSignIn();
  };

  const handleToggleLogin = () => {
    toggleLogin(!isLogin);
  };

  return (
    <Form
      onSubmit={() => null}
      validate={validate}
      render={({ handleSubmit }) => (
        <form onSubmit={handleSubmit} noValidate>
          {isLogin ? (
            <div>
              <div className="field-form">
                <Input
                  heading="Email"
                  name="email"
                  required
                  setItem={handleChange}
                  value={defaultValues.email}
                />
              </div>
              {hasEmail && (
                <div className="field-form">
                  <Input
                    heading="Pin"
                    name="pin"
                    type="password"
                    value={defaultValues.pin}
                    setItem={handleChange}
                    disabled={pinDisabled}
                  />
                </div>
              )}
            </div>
          ) : (
            <div>
              <div className="field-form">
                <DropZone
                  name="multipassZip"
                  maxFiles={1}
                  accept=".zip,.rar,.7zip/*"
                  setItem={handleChange}
                  removeItem={deleteFile}
                  required
                />
              </div>
              <div className="field-form">
                <PasswordInput
                  heading="Password"
                  name="password"
                  value={defaultValues.password}
                  setItem={handleChange}
                  required
                />
              </div>
              {!pinDisabled && (
                <div>
                  <div className="field-form">
                    <Input
                      name="pin"
                      placeholder="Enter your pin"
                      type="password"
                      value={defaultValues.pin}
                      setItem={handleChange}
                      disabled={pinDisabled}
                    />
                  </div>
                  <div className="field-form">
                    <Input
                      placeholder="Email"
                      name="email"
                      disabled
                      value={defaultValues.email}
                    />
                  </div>
                </div>
              )}
            </div>
          )}
          <div>
            <div className="justify-between list-columns">
              {!isLogin && (
                <div className="column-item" style={{ marginRight: 5 }}>
                  <Button
                    disabled={isLogin}
                    onClick={onSubmitSignUp}
                    text="Register&SignIn"
                    className="block-button"
                  />
                </div>
              )}
              <div className="column-item">
                <Button
                  onClick={handleClickSignIn}
                  bgColor="#3463F8"
                  text={
                    (isLogin && hasEmail) || !isLogin ? "Sign in" : "Check email"
                  }
                  className="block-button"
                />
              </div>
            </div>

            <div className="margin-top-10">
              <Button
                onClick={handleToggleLogin}
                bgColor="#b5b5b500"
                color="#00a8ff"
                text={!isLogin ? "Login by email" : "To the registration"}
                className="block-button"
              />
            </div>
          </div>
        </form>
      )}
    />
  );
};

FormQR.defaultProps = {
  defaultValues: {
    email: "",
    password: "",
  },
};
