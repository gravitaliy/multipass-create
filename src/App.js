import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { CreatePass, CheckPass, QRAuth } from "./routes";
import { Header } from "./containers";
import { Loader, NotificationComponent } from "./components";
import { RootStore } from "./stores/rootStore";
import { routesList } from "./assets/enums";
import "./App.scss";

const store = new RootStore();

export const App = () => {
  return (
    <Router>
      <NotificationComponent store={store.notificationStore} />
      <Loader store={store.loaderStore} />
      <Header />
      <Switch>
        <Route
          exact
          path="/"
          component={() => (
            <CreatePass store={store} title="Multipass: create passport" />
          )}
        />
        <Route
          path={routesList.CHECK_PASSPORT}
          render={() => (
            <CheckPass store={store} title="Multipass: check passport" />
          )}
        />
        {/* <Route
          path={routesList.PARTIAL_PASSPORT}
          render={() => (
            <PartialPass
              title="Multipass: create partial passport"
              store={store.multipassForm}
            />
          )}
        /> */}
        <Route
          path={routesList.QR_AUTH_DEMO}
          render={() => (
            <QRAuth store={store} title="Multipass: QR auth demo" />
          )}
        />
      </Switch>
    </Router>
  );
};
