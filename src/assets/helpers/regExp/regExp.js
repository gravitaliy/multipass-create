export const cronRegex = "A[0-9a-zA-Z]{33}";
export const latinRegex = "^[a-zA-Z]+$";
export const latinAndNumbersRegex = "^[a-zA-Z0-9]+$";
export const latinAndNumbersWithSpaceRegex = "^[a-zA-Z0-9 -.,]+$";
export const emailRegex =
  "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$";
export const passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*d)[a-zA-Zd]{8,}$";
