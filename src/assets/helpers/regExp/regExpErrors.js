export const onlyLatinError = "Latin letters only";
export const onlyLatinAndNumbersError = "Latin letters and numbers only";
export const passwordError =
  "Minimum eight characters, at least one letter, one number and one special character";
export const emailError = "Valid email address";
