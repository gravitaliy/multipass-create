export const useTitle = title => {
  return title && (document.title = title);
};
