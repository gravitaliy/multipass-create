import { encrypt, decrypt, PrivateKey } from "eciesjs";

const key = new PrivateKey();

export const decryptData = data => {
  try {
    const decryptedData = decrypt(key.toHex(), data);
    return decryptedData;
  } catch (error) {
    console.error(error);
  }
};

export const encryptData = (publicKey, data) => {
  const newData = new Uint8Array(data);
  try {
    const encryptedData = encrypt(publicKey, newData);
    return encryptedData;
  } catch (error) {
    console.error(error);
  }
};
