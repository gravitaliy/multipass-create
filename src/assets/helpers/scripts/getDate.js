export const getDateNearThis = offset => {
  return new Date(new Date().setDate(new Date().getDate() + offset));
};
