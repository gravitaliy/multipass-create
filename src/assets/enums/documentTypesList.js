export const documentTypesList = [
  { value: "id_card", label: "Id card", id: 1 },
  { value: "driving_license", label: "Driving license", id: 2 },
  { value: "national_passport", label: "National passport", id: 3 },
  { value: "diplomatic_passport", label: "Diplomatic passport", id: 4 },
  {
    value: "seafarer_discharge_book",
    label: "Seafarer’s discharge book",
    id: 5,
  },
  { value: "employer_letter", label: "Emloyer", id: 6 },
  { value: "insurance_agreement", label: "Insurance agreement", id: 7 },
  { value: "other", label: "Other", id: 8 },
];
