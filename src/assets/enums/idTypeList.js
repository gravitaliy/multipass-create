export const idTypeList = [
  { value: "foreign_passport", label: "Foreign Passport", id: 1 },
  { value: "id_card", label: "Identity card", id: 2 },
  { value: "internal_passport", label: "Internal passport", id: 3 },
];
