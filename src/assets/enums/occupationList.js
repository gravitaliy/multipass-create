export const occupationList = [
  { value: "employee", label: "Employee", id: 1 },
  { value: "individual", label: "Individual", id: 2 },
  { value: "student", label: "Student", id: 3 },
  { value: "pensioner", label: "Pensioner", id: 4 },
  { value: "unemployed", label: "Unemployed", id: 5 },
  { value: "self_employed", label: "Self-employed", id: 6 },
  { value: "other", label: "Other", id: 7 },
];
