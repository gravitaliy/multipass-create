export const routesList = {
  CREATE_PASSPORT: "/",
  CHECK_PASSPORT: "/CheckPassport",
  PARTIAL_PASSPORT: "/PartialPassport",
  QR_AUTH_DEMO: "/QRAuth",
};
