import React from "react";
import { routesList } from "../../assets/enums";
import { Logo } from "../../components";
import "./Header.scss";

const navbar = [
  { label: "Create Passport", link: routesList.CREATE_PASSPORT },
  { label: "Check Passport", link: routesList.CHECK_PASSPORT },
  // {
  //   label: "Create partial passport",
  //   link: routesList.PARTIAL_PASSPORT,
  // },
  { label: "QRAuth demo", link: routesList.QR_AUTH_DEMO },
].map((item, i) => ({ ...item, id: i }));

export const Header = () => {
  return (
    <div className="navbar-wrap">
      <div className="navbar flex start align-center">
        <Logo />
        {navbar.map(item => (
          <a className="navbar-item" href={item.link} key={item.id}>
            {item.label}
          </a>
        ))}
      </div>
    </div>
  );
}
