import { observable } from "mobx";
import { checkMultipassActions } from "./checkMultipassActions";

export class CheckMultipassStore extends checkMultipassActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  @observable multipass = {
    multipassZip: "",
    password: "",
  };

  setMultipass(name, value) {
    this.multipass[name] = value;
  }

  deleteMultipass() {
    this.setMultipass("multipassZip", "");
  }
}
