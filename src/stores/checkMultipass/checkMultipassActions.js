import { MultipassReader, MultipassConstants } from "multipass-file-beta";

export class checkMultipassActions {
  isMultipassUploaded() {
    return this.multipass.multipassZip !== "";
  }

  async unpackMultipass(showFields) {
    this.rootStore.loaderStore.startLoading();

    try {
      const { password, multipassZip } = this.multipass;

      const multipass = await this.multipassToArray(multipassZip);
      const multipassUnpacked = new MultipassReader(multipass, password);
      const metaPath = multipassUnpacked.listMetaFiles()[0];
      const meta = multipassUnpacked.readMeta(metaPath);

      const actualFields = Object.keys(meta.Fields);
      const resultFields = this.getFields(
        multipassUnpacked,
        actualFields,
        password,
      );
      showFields("success", resultFields);
      this.rootStore.loaderStore.stopLoading();
    } catch (err) {
      this.rootStore.loaderStore.stopLoading();
      showFields("error");
      throw err;
    }
  }

  getFields = (multipass, actualFields, password) => {
    const { fieldsName } = multipass;
    const resultFields = actualFields.map(FieldId => {
      let fieldValue;
      const fieldName = fieldsName[FieldId.split("-")[0]];
      const fieldType = MultipassConstants.fields[fieldName].type;
      if (fieldType === "File") {
        fieldValue = multipass.readField({ FieldId, Index: "0" }, password)
          .file;
      } else {
        fieldValue = multipass.readField({ FieldId, Index: "0" }, password)
          .value;
      }
      if (fieldType === "File") {
        const blob = new Blob([fieldValue], { type: "image/jpeg" });
        const imageUrl = URL.createObjectURL(blob);
        return { type: "file", value: imageUrl, name: fieldName };
      }
      return {
        type: "string",
        value: fieldType === "Date" ? fieldValue.split("T")[0] : fieldValue,
        name: fieldName,
      };
    });

    return resultFields;
  };

  multipassToArray = async multipass => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsArrayBuffer(multipass);
      reader.onloadend = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  };
}
