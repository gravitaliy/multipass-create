import { observable } from "mobx";

export class LoaderStore {
  @observable loader = {
    isLoading: false,
    text: "Loading...",
  };

  startLoading() {
    this.loader.isLoading = true;
  }

  stopLoading() {
    this.loader.isLoading = false;
  }

  setText(text) {
    this.loader.text = text;
  }
}
