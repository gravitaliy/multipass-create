import { saveAs } from "file-saver";
import { MultipassCreator, MultipassConstants } from "multipass-file-beta";

export class MultipassFormActions {
  getDocumentError = document => ({
    title: "Please, upload file!",
    message: `Upload ${document} document`,
  });

  checkDocumentUploading() {
    this.documenErrors = [];
    let checkResult = true;
    if (this.firstStep.IdFile === "") {
      checkResult = false;
      this.documenErrors.push(this.getDocumentError("Passport/Id card"));
    }
    if (
      this.firstStep.IdType !== "Foreign Passport" &&
      this.firstStep.BackOfIdCard === ""
    ) {
      checkResult = false;
      this.documenErrors.push(this.getDocumentError("Id card backside"));
    }
    if (this.firstStep.Selfie === "") {
      checkResult = false;
      this.documenErrors.push(
        this.getDocumentError("Selfie with passport / ID card"),
      );
    }
    if (this.secondStep.ResAddressProofId === "") {
      checkResult = false;
      this.documenErrors.push(this.getDocumentError("Proof of address"));
    }

    if (this.secondStep.ResAddressProofAdd === "") {
      checkResult = false;
      this.documenErrors.push(
        this.getDocumentError("Additional proof of address"),
      );
    }

    if (
      this.fourthStep.AddIdNumber1 !== "" &&
      this.fourthStep.AddFile1 === ""
    ) {
      checkResult = false;
      this.documenErrors.push(this.getDocumentError("Additional"));
    }
    return checkResult;
  }

  fixAllDates() {
    this.firstStep.DateOfBirth = this.toYYYYMMDD(this.firstStep.DateOfBirth);
    this.firstStep.DateOfIdIssuing = this.toYYYYMMDD(
      this.firstStep.DateOfIdIssuing,
    );
    this.firstStep.DateOfIdExpiry = this.toYYYYMMDD(
      this.firstStep.DateOfIdExpiry,
    );
    this.fourthStep.AddDateIdIssuing1 = this.toYYYYMMDD(
      this.fourthStep.AddDateIdIssuing1,
    );
    this.fourthStep.AddDateIdExpiry1 = this.toYYYYMMDD(
      this.fourthStep.AddDateIdExpiry1,
    );
  }

  toYYYYMMDD(date) {
    if (date instanceof Date) {
      return date.toISOString().split("T")[0];
    }
    return date;
  }

  async createMultipass() {
    // this.rootStore.loaderStore.startLoading();
    /* if (!this.checkDocumentUploading()) {
      this.rootStore.loaderStore.stopLoading();
      return this.documenErrors;
    } */
    // this.fixAllDates();

    const multipass = new MultipassCreator(
      this.fifthStep.password,
      this.fifthStep.CronAddress,
    );
    [this.firstStep, this.secondStep, this.thirdStep, this.fourthStep].forEach(
      multipassData => {
        Object.keys(multipassData).forEach(key => {
          if (multipassData[key] !== "") {
            if (MultipassConstants?.fields[key]?.type === "File") {
              multipass.addField(
                key,
                multipassData[key],
                0,
                multipassData[key]?.type,
              );
            } else {
              multipass.addField(key, multipassData[key], 0);
            }
          }
        });
      },
    );
    multipass.addAuth("main", this.fifthStep?.password);
    saveAs(
      new File([multipass.generate()], "multipass.zip", {
        type: "application/octet-binary",
      }),
    );

    // this.rootStore.loaderStore.stopLoading();
    return true;
  }
}
