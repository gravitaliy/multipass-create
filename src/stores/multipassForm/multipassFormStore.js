import { observable } from "mobx";
import { MultipassFormActions } from "./multipassFormActions";
import { getDateNearThis } from "../../assets/helpers";

export class MultipassFormStore extends MultipassFormActions {
  constructor(rootStore) {
    super();
    this.rootStore = rootStore;
  }

  @observable firstStep = {
    IntName: "",
    IntSurname: "",
    Name: "",
    Surname: "",
    MiddleName: "",
    Gender: "",
    Citizenship: "",
    Occupation: "",
    OccupationOther: "",
    DateOfBirth: getDateNearThis(-1),
    PlaceOfBirth: "",
    IdNumber: "",
    IssuedBy: "",
    DateOfIdIssuing: getDateNearThis(-1),
    DateOfIdExpiry: new Date(),
    IdFile: "",
    BackOfIdCard: "",
    Selfie: "",
    IdType: "",
  };

  @observable secondStep = {
    ResCountry: "",
    ResRegion: "",
    ResCity: "",
    ResPostalCode: "",
    ResAddress: "",
    ResDocType: "",
    ResAddressProofId: "",
    ResAddressProofAdd: "",
  };

  @observable thirdStep = {
    Phone1: "",
    EMail: "",
  };

  @observable fourthStep = {
    AddDocType1: "",
    AddIssuingCountry1: "",
    AddIdNumber1: "",
    AddDateIdIssuing1: getDateNearThis(-1),
    AddDateIdExpiry1: new Date(),
    AddFile1: "",
  };

  @observable fifthStep = {
    password: "",
    CronAddress: "",
  };

  setFirstStepItem(name, value) {
    this.firstStep[name] = value;
  }

  setSecondStepItem(name, value) {
    this.secondStep[name] = value;
  }

  setThirdStepItem(name, value) {
    this.thirdStep[name] = value;
  }

  setFourthStepItem(name, value) {
    this.fourthStep[name] = value;
  }

  setFifthStepItem(name, value) {
    this.fifthStep[name] = value;
  }

  documenErrors = [];
}
