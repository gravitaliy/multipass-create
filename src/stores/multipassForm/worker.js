import { MultipassCreator } from "multipass-file-beta";

onmessage = ({ data }) => {
  const multipassAuthor = data[0];
  const multipassDataArray = data[1];
  const multipass = new MultipassCreator(
    multipassAuthor.password,
    multipassAuthor.CronAddress,
  );
  multipassDataArray.forEach(multipassData => {
    Object.keys(multipassData).forEach(key => {
      if (multipassData[key] !== "") {
        multipass.addField(key, multipassData[key]);
      }
    });
  });
  postMessage(multipass.generate());
};
