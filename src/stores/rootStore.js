/* eslint-disable import/extensions */
import { MultipassFormStore } from "./multipassForm/multipassFormStore";
import { CheckMultipassStore } from "./checkMultipass/checkMultipassStore";
import { LoaderStore } from "./loader/loader.store.js";
import { NotificationStore } from "./notification/notification";

export class RootStore {
  constructor() {
    this.multipassForm = new MultipassFormStore(this);
    this.checkMultipass = new CheckMultipassStore(this);
    this.loaderStore = new LoaderStore(this);
    this.notificationStore = new NotificationStore(this);
  }
}
